cmake_minimum_required (VERSION 3.15)

project (googletest)

set (GOOGLE_TEST_VERSION e2f3978)
set (GOOGLE_TEST_LIBRARY ${PROJECT_NAME})
set (GOOGLE_MOCK_LIBRARY googlemock)

add_library (
    ${GOOGLE_TEST_LIBRARY}
    ${CMAKE_CURRENT_SOURCE_DIR}/${GOOGLE_TEST_VERSION}/googletest/src/gtest-all.cc
)

target_include_directories (
    ${GOOGLE_TEST_LIBRARY}
    SYSTEM PUBLIC
        ${CMAKE_CURRENT_SOURCE_DIR}/${GOOGLE_TEST_VERSION}/googletest/include
    PRIVATE
        ${CMAKE_CURRENT_SOURCE_DIR}/${GOOGLE_TEST_VERSION}/googletest
)

add_library (
    ${GOOGLE_MOCK_LIBRARY}
    ${CMAKE_CURRENT_SOURCE_DIR}/${GOOGLE_TEST_VERSION}/googlemock/src/gmock-all.cc
)

target_include_directories (
    ${GOOGLE_MOCK_LIBRARY}
    SYSTEM PUBLIC
        ${CMAKE_CURRENT_SOURCE_DIR}/${GOOGLE_TEST_VERSION}/googlemock/include
    PRIVATE
        ${CMAKE_CURRENT_SOURCE_DIR}/${GOOGLE_TEST_VERSION}/googlemock
)

target_link_libraries (
    ${GOOGLE_MOCK_LIBRARY} PUBLIC
    ${GOOGLE_TEST_LIBRARY}
)

add_library (Google::GoogleTest ALIAS ${GOOGLE_TEST_LIBRARY})
add_library (Google::GoogleMock ALIAS ${GOOGLE_MOCK_LIBRARY})
