#include "signal.hh"

#include <thread>

#include <gtest/gtest.h>

namespace
{
    constexpr auto EXPECTED_INT_VALUE = int{ 5 };

    auto slot_called = false;

    struct S
    {
        constexpr explicit S(int data_value = 0)
          : data{ data_value }
        {}

        inline ~S() = default;

        S(const S &other)
          : data{ other.data }
        {
            ++copy_construct_count;
        }

        S(S &&other)
        noexcept
          : data{ other.data }
        {
            ++move_construct_count;
            other.data = 0;
        }

        S &operator=(const S &lhs)
        {
            ++copy_assignment_count;

            data = lhs.data;

            return *this;
        }

        S &operator=(S &&lhs) noexcept
        {
            ++move_assignment_count;

            data     = lhs.data;
            lhs.data = 0;

            return *this;
        }

        int data{ 0 };

        inline static std::size_t move_construct_count{ 0 };
        inline static std::size_t move_assignment_count{ 0 };
        inline static std::size_t copy_construct_count{ 0 };
        inline static std::size_t copy_assignment_count{ 0 };

        static void reset_counts() noexcept
        {
            move_construct_count  = 0;
            move_assignment_count = 0;
            copy_construct_count  = 0;
            copy_assignment_count = 0;
        }
    };
} // namespace

template<typename... Args> using Signal = elsen::Signal<Args...>;
using SignalConnectionType              = elsen::signal::ConnectionType;

TEST(signal, connect_member_function)
{
    auto signal = Signal<int>{};

    struct TestClass
    {
        void slot(int value)
        {
            slot_called = true;
            ASSERT_EQ(value, EXPECTED_INT_VALUE);
        }

        bool slot_called{ false };
    };

    auto test_object = TestClass{};

    signal.connect<&TestClass::slot>(test_object);
    signal.emit(EXPECTED_INT_VALUE);

    ASSERT_TRUE(test_object.slot_called);
}

TEST(signal, connect_queued_member_function)
{
    auto signal = Signal<int>{};

    struct TestClass
    {
        void slot(int value)
        {
            slot_called = true;
            ASSERT_EQ(value, EXPECTED_INT_VALUE);
        }

        bool slot_called{ false };
    };

    auto test_object = TestClass{};

    signal.connect<&TestClass::slot>(test_object, SignalConnectionType::Queued);
    signal.emit(EXPECTED_INT_VALUE);

    std::this_thread::sleep_for(std::chrono::milliseconds{ 10 });

    ASSERT_TRUE(test_object.slot_called);
}

static void test_signal_connection(int value)
{
    slot_called = true;
    ASSERT_EQ(value, EXPECTED_INT_VALUE);
}

TEST(signal, connect_free_function)
{
    slot_called = false;

    auto signal = Signal<int>{};
    signal.connect(&test_signal_connection);
    signal.emit(EXPECTED_INT_VALUE);

    ASSERT_TRUE(slot_called);
}

TEST(signal, connect_queued_free_function)
{
    slot_called = false;

    auto signal = Signal<int>{};
    signal.connect(&test_signal_connection, SignalConnectionType::Queued);
    signal.emit(EXPECTED_INT_VALUE);

    std::this_thread::sleep_for(std::chrono::milliseconds{ 10 });

    ASSERT_TRUE(slot_called);
}

TEST(signal, connect_stateless_lambda)
{
    struct State
    {
        bool slot_called{ false };
    };
    auto state = State{};

    auto signal = Signal<int>{};
    signal.connect(
        [](int value, State *s) {
            ASSERT_EQ(value, EXPECTED_INT_VALUE);
            s->slot_called = true;
        },
        state);
    signal.emit(EXPECTED_INT_VALUE);

    ASSERT_TRUE(state.slot_called);
}

TEST(signal, connect_queued_stateless_lambda)
{
    struct State
    {
        bool slot_called{ false };
    };
    auto state = State{};

    auto signal = Signal<int>{};
    signal.connect(
        [](int value, State *s) {
            ASSERT_EQ(value, EXPECTED_INT_VALUE);
            s->slot_called = true;
        },
        state,
        SignalConnectionType::Queued);
    signal.emit(EXPECTED_INT_VALUE);

    std::this_thread::sleep_for(std::chrono::milliseconds{ 10 });

    ASSERT_TRUE(state.slot_called);
}

TEST(signal, struct_connect_member_function)
{
    S::reset_counts();

    auto signal = Signal<S>{};

    struct TestClass
    {
        void slot(S s)
        {
            slot_called = true;
            ASSERT_EQ(s.data, EXPECTED_INT_VALUE);
        }

        bool slot_called{ false };
    };

    auto test_object = TestClass{};

    signal.connect<&TestClass::slot>(test_object);
    signal.emit(S{ EXPECTED_INT_VALUE });

    ASSERT_TRUE(test_object.slot_called);

    ASSERT_TRUE(S::move_construct_count == 2);
    ASSERT_TRUE(S::move_assignment_count == 0);
    ASSERT_TRUE(S::copy_construct_count == 0);
    ASSERT_TRUE(S::copy_assignment_count == 0);
}

TEST(signal, struct_ref_connect_member_function)
{
    static void *s_address = nullptr;
    S::reset_counts();

    auto signal = Signal<S &>{};

    auto s    = S{};
    s_address = &s;

    struct TestClass
    {
        void slot(S &s)
        {
            slot_called = true;
            ASSERT_TRUE(s_address == &s);
        }

        bool slot_called{ false };
    };

    auto test_object = TestClass{};

    signal.connect<&TestClass::slot>(test_object);
    signal.emit(s);

    ASSERT_TRUE(test_object.slot_called);

    ASSERT_TRUE(S::move_construct_count == 0);
    ASSERT_TRUE(S::move_assignment_count == 0);
    ASSERT_TRUE(S::copy_construct_count == 0);
    ASSERT_TRUE(S::copy_assignment_count == 0);
}

TEST(signal, struct_const_ref_connect_member_function)
{
    static const void *s_address = nullptr;
    S::reset_counts();

    auto signal = Signal<const S &>{};

    const auto s = S{};
    s_address    = &s;

    struct TestClass
    {
        void slot(const S &s)
        {
            slot_called = true;
            ASSERT_TRUE(s_address == &s);
        }

        bool slot_called{ false };
    };

    auto test_object = TestClass{};

    signal.connect<&TestClass::slot>(test_object);
    signal.emit(s);

    ASSERT_TRUE(test_object.slot_called);

    ASSERT_TRUE(S::move_construct_count == 0);
    ASSERT_TRUE(S::move_assignment_count == 0);
    ASSERT_TRUE(S::copy_construct_count == 0);
    ASSERT_TRUE(S::copy_assignment_count == 0);
}

TEST(signal, struct_rval_ref_connect_member_function)
{
    S::reset_counts();

    auto signal = Signal<S &&>{};

    struct TestClass
    {
        void slot(S &&s)
        {
            slot_called = true;
            ASSERT_TRUE(s.data == EXPECTED_INT_VALUE);
        }

        bool slot_called{ false };
    };

    auto test_object = TestClass{};

    signal.connect<&TestClass::slot>(test_object);
    signal.emit(S{ EXPECTED_INT_VALUE });

    ASSERT_TRUE(test_object.slot_called);

    ASSERT_TRUE(S::move_construct_count == 0);
    ASSERT_TRUE(S::move_assignment_count == 0);
    ASSERT_TRUE(S::copy_construct_count == 0);
    ASSERT_TRUE(S::copy_assignment_count == 0);
}

TEST(signal, emit_no_connection)
{
    Signal<> signal{};
    signal.emit();
}
