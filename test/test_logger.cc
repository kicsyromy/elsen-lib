#include <elsen/logger.hh>

#include <string_view>

#include <gtest/gtest.h>

TEST(logger, format_specifier)
{
    constexpr auto fmt_specifier =
        elsen::Logger::construct_format_specifier("/path/to/test/source/file.cc", "111", "{:x}");

    ASSERT_TRUE(
        std::string_view{ fmt_specifier.data() } == std::string_view{ "file.cc:111: {:x}" });
}