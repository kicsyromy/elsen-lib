#include <elsen/signal.hh>

#include <gtest/gtest.h>

#if defined(ELSEN_PLATFORM_X86_64) || defined(ELSEN_PLATFORM_I386)
int main(int argc, char *argv[])
#else
int the_main(int argc, char *argv[])
#endif
{
    ::testing::InitGoogleTest(&argc, argv);
    elsen::signal::platform_init_queued_signal_handler();

    return RUN_ALL_TESTS();
}

#ifdef ELSEN_PLATFORM_XTENSA
#include <esp_event.h>

#include <array>

extern "C" [[noreturn]] void app_main()
{
    ESP_ERROR_CHECK(esp_event_loop_create_default());

    auto argv = std::array{ "elsen_test" };
    the_main(static_cast<int>(argv.size()), const_cast<char **>(argv.data()));

    for (;;)
    {
        vTaskDelay(portMAX_DELAY);
    }
}
#endif